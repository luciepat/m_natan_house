#include "House.h"


int change[5] = { 0,1,-1,3,-3 };// same, next, before, up, down
char ** code_list = NULL;
int len = 0;

/*
recursive function that adds to code_list every option from codepad
in: code and the placement of the num to change
out: none
*/
void get_code_loop(char* code, size_t place)
{
	for (size_t i = 0; i < 5; i++) // for all max of 5 numbers a number can be
	{
		if (code[place] + change[i] > ASCII_BEFORE_1 && code[place] + change[i] < ASCII_AFTER_9) { //if option is legal (EG 1 down)
			code[place] += change[i];
			if (place < strlen(code) - 1) { // if isn't last number in code recurse
				get_code_loop(code, place + 1);
			}
			else {
				add_option(code);
			}
			code[place] -= change[i];
		}
	}
}

char** get_code_list(char * code)
{
	get_code_loop(code, 0);
	return code_list;
}

/*
	adds code option to code_list (deals with mem)
	in: code
	out: none
*/
void add_option(char * code){
	char* _new = (char*)malloc(sizeof(char)*(strlen(code) + 1));// (+1 needs to include NULL) 
	if (!_new) {
		freer();
		exit(1);
	}
	strcpy_s(_new, strlen(code)+1, code);//fills new with this option
	void* temp = (char**)realloc(code_list, sizeof(char*)*(len + 1)); // temp is for fail. can't lose access to mem, need to free
	if (!temp) {
		freer();
		exit(1);
	}
	code_list = (char**)temp;
	code_list[len] = _new;
	len++;
}



void freer()
{
	for (int i = len - 1; i >= 0; i--) {
		free(code_list[i]);
	}
	free(code_list);
}
	



