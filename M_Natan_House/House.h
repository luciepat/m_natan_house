#pragma once
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define ASCII_BEFORE_1 47
#define ASCII_AFTER_9 58
char** get_code_list(char* code);
void get_code_loop(char* code, size_t place);
void add_option(char* code);
void freer();
